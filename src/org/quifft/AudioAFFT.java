package org.quifft;

import org.quifft.audioread.AudioReader;
import org.quifft.audioread.AudioReaderFactory;
import org.quifft.fft.FFTComputationWrapper;
import org.quifft.output.*;
import org.quifft.params.ParametrosFFT;
import org.quifft.params.ParameterValidator;
import org.quifft.params.Tipo_Onda;
import org.quifft.sampling.SampleWindowExtractor;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

/**
 * Class used by the client to compute an FFT for an audio file
 */
public class AudioAFFT {

    // parameters for FFT operation (i.e. window size, normalization, etc)
    private ParametrosFFT parametros = new ParametrosFFT();

    // audio reader for input file
    private AudioReader lectordeAudio;

    /**
     * Constructs a QuiFFT instance with an archivoAudio file
     * @param archivoAudio reference to archivoAudio file for which FFT will be performed
     * @throws IOException if an I/O exception occurs when the input stream is initialized
     * @throws UnsupportedAudioFileException if the file is not a valid archivoAudio file or has bit depth greater than 16
     */
    public AudioAFFT(File archivoAudio) throws IOException, UnsupportedAudioFileException {
        this.lectordeAudio = AudioReaderFactory.audioReaderFor(archivoAudio);
    }

    /**
     * Constructs a QuiFFT instance with a String file name
     * @param fileName name of audio file for which FFT will be performed
     * @throws IOException if an I/O exception occurs when the input stream is initialized
     * @throws UnsupportedAudioFileException if the file is not a valid audio file or has bit depth greater than 16
     */
    public AudioAFFT(String archivoAudio) throws IOException, UnsupportedAudioFileException {
        this(new File(archivoAudio));
    }

    /**
     * Set window size (number of samples per FFT)
     * <p>If DefineNumeroDePuntos parameter is not defined, this must be a power of 2.</p>
     * @param tamanoVentana number of samples from audio file for which each FFT should be performed
     * @return current AudioAFFT object with window size parameter set
     */
    public AudioAFFT windowSize(int tamanoVentana) {
        parametros.TamanoVentanaMuestreo = tamanoVentana;
        return this;
    }

    /**
     * Get window size parameter for FFT
     * @return window size parameter for FFT
     */
    public int ObtenerTamanoVentanaMuestreo() {
        return parametros.TamanoVentanaMuestreo;
    }

    /**
     * Set window function to be used for obtaining sequence of samples to be used for each FFT frame
     * @param funcion type of window function to be used
     * @return current AudioAFFT object with window function parameter set
     */
    public AudioAFFT SeleccionaFuncion(Tipo_Onda funcion) {
        parametros.tipoOnda = funcion;
        return this;
    }

    /**
     * Get window function parameter for FFT
     * @return window function parameter for FFT
     */
    public Tipo_Onda ObtenFuncion() {
        return parametros.tipoOnda;
    }

    /**
     * Set percentage by which adjacent windows should be overlapped
     * @param porcentaje_Traslape value between 0 and 1 representing window overlap percentage
     * @return current AudioAFFT object with window overlap percentage parameter set
     */
    public AudioAFFT DefineTraslapeDeVentana(double porcentaje_Traslape) {
        parametros.TraslapeDeVentana = porcentaje_Traslape;
        return this;
    }

    /**
     * Get window overlap percentage parameter for FFT
     * @return window overlap percentage parameter for FFT
     */
    public double ObtenTraslapeDeVentana() {
        return parametros.TraslapeDeVentana;
    }

    /**
     * Set number of points for FFT
     * <p>If this is not explicitly defined, the number of points will be equal to the window size.
 If defined, must be greater than or equal to window size AND must be a power of 2.
 Each signal window will be zero-padded to reach a length equal to DefineNumeroDePuntos.</p>
     * @param numeroPuntos the number of points for the N-point FFT
     * @return current AudioAFFT object with number of points parameter set
     */
    public AudioAFFT DefineNumeroDePuntos(int numeroPuntos) {
        parametros.NumeroDePuntos = numeroPuntos;
        return this;
    }

    /**
     * Get number of points parameter for FFT
     * @return number of points parameter for FFT
     */
    public int ObtenNumeroDePuntos() {
        return parametros.NumeroDePuntos;
    }

    /**
     * Set whether the amplitudes of each frequency bin should be scaled logarithmically (decibels) instead of linearly
     * @param usarEscalaDecibeles true if amplitudes should be dB scale, false for linear scale
     * @return current AudioAFFT object with amplitude scaling parameter set
     */
    public AudioAFFT DefineEscala(boolean usarEscalaDecibeles) {
        parametros.UsarEscalaDecibeles = usarEscalaDecibeles;
        return this;
    }

    /**
     * Get amplitude scaling parameter for FFT
     * @return true if amplitudes will be logarithmically scaled (decibels), false if they'll be on a linear scale
     */
    public boolean UsaDecibeles() {
        return parametros.UsarEscalaDecibeles;
    }

    /**
     * Set option for whether FFT amplitudes should be DefineNormalizado (scaled to range from 0 to 1)
     * @param normalizar true if FFT results should be DefineNormalizado
     * @return current AudioAFFT object with normalization parameter set
     */
    public AudioAFFT DefineNormalizado(boolean normalizar) {
        parametros.EstaNormalizado = normalizar;
        return this;
    }

    /**
     * Get normalization parameter for FFT
     * @return true if FFT amplitudes will be DefineNormalizado
     */
    public boolean EsNormalizado() {
        return parametros.EstaNormalizado;
    }

    /**
     * Performs an FFT for the entirety of the audio file
     * @return an FFT result containing metadata of this FFT and an array of all {@link CuadroFFT}s computed
     * @throws BadParametersException if there are any invalid FFT parameters set
     */
    public ResultadosFFT CalcularFFT() {
        ParameterValidator.validateFFTParameters(parametros, false);

        ResultadosFFT resultados = new ResultadosFFT();
        resultados.setMetadata(lectordeAudio, parametros);  // carga metadatos

        boolean esEstereo = lectordeAudio.ObtenFormatoDeAudio().getChannels() == 2;
        float frecuenciaMuestreo = lectordeAudio.ObtenFormatoDeAudio().getSampleRate();
        int[] formaDeOnda = lectordeAudio.getWaveform();

        int cantMuestras = formaDeOnda.length / (esEstereo ? 2 : 1);
        double multilicadorTraslape = 1 / (1 - parametros.TraslapeDeVentana);
        int numCuadros = (int) Math.ceil(((double) cantMuestras / parametros.TamanoVentanaMuestreo) * multilicadorTraslape);
        CuadroFFT[] cuadrosFFT = new CuadroFFT[numCuadros];

        SampleWindowExtractor windowExtractor = new SampleWindowExtractor(formaDeOnda, esEstereo, parametros.TamanoVentanaMuestreo,
                parametros.tipoOnda, parametros.TraslapeDeVentana, parametros.zeroPadLength());
        double currentAudioTimeMs = 0;
        for(int i = 0; i < cuadrosFFT.length; i++) {
            // sampleWindow is input to FFT -- may be zero-padded if DefineNumeroDePuntos > TamanoVentanaMuestreo
            int[] sampleWindow = windowExtractor.extractWindow(i);

            // compute next current FFT frame
            cuadrosFFT[i] = FFTComputationWrapper.doFFT(sampleWindow, currentAudioTimeMs, resultados.windowDurationMs,
                    resultados.fileDurationMs, frecuenciaMuestreo, parametros);

            // adjust current audio time
            currentAudioTimeMs += resultados.windowDurationMs * (1 - parametros.TraslapeDeVentana);
        }

        if(parametros.UsarEscalaDecibeles) {
            FFTComputationWrapper.scaleLogarithmically(cuadrosFFT);
        } else if(parametros.EstaNormalizado) {
            double maxAmplitude = findMaxAmplitude(cuadrosFFT);
            normalizeFFTResult(cuadrosFFT, maxAmplitude);
        }

        resultados.fftFrames = cuadrosFFT;
        return resultados;
    }

    /**
     * Creates an FFTStream which can be used as an iterator to compute FFT frames one by one
     * @return an FFTStream which can be used as an iterator to compute FFT frames one by one
     * @throws BadParametersException if there are any invalid FFT parameters set
     */
    public FFTStream fftStream() {
        ParameterValidator.validateFFTParameters(parametros, true);

        FFTStream fftStream = new FFTStream();
        fftStream.setMetadata(lectordeAudio, parametros);

        return fftStream;
    }

    /**
     * Normalizes each bin amplitude by dividing all amplitudes by the max amplitude
     * @param fftFrames array of frames obtained by an FFT operation
     */
    private void normalizeFFTResult(CuadroFFT[] fftFrames, double maxAmp) {
        for(CuadroFFT frame : fftFrames) {
            for(FrequencyBin bin : frame.bins) {
                bin.amplitude /= maxAmp;
            }
        }
    }

    /**
     * Returns the maximum amplitude of any frequency bin in any frame in an array of FFTFrames
     * @param fftFrames an array of FFTFrames obtained by an FFT operation
     * @return the maximum amplitude found in the array of FFTFrames
     */
    private static double findMaxAmplitude(CuadroFFT[] fftFrames) {
        double maxAmp = 0;
        for(CuadroFFT frame : fftFrames) {
            for(FrequencyBin bin : frame.bins) {
                maxAmp = Math.max(maxAmp, bin.amplitude);
            }
        }
        return maxAmp;
    }

}
