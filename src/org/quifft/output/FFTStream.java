package org.quifft.output;

import org.quifft.audioread.AudioReader;
import org.quifft.fft.FFTComputationWrapper;
import org.quifft.params.ParametrosFFT;
import org.quifft.params.Tipo_Onda;
import org.quifft.sampling.SampleWindowExtractor;

import java.util.Iterator;

/**
 * FFTStream computes an FFT on an audio file incrementally as opposed to all at once
 * <p>It exposes an Iterator interface for computing {@link CuadroFFT}s one at a time.
 * This can be a useful alternative to {@link FFTResult} if your audio file is large or you are space-constrained.</p>
 */
public class FFTStream extends FFTOutputObject implements Iterator<CuadroFFT> {

    // AudioReader from which samples can be extracted
    private AudioReader audioReader;

    // Counter for how many samples have been computed so far (how many times next() has been called)
    private int samplesTakenCount;

    /**
     * Checks whether another CuadroFFT exists
     * @return true if another CuadroFFT exists
     */
    public boolean hasNext() {
        return audioReader.hasNext();
    }

    /**
     * Gets next computed CuadroFFT
     * @return next computed CuadroFFT
     */
    public CuadroFFT next() {
        int[] nextWindow = audioReader.next();
        boolean isStereo = audioReader.ObtenFormatoDeAudio().getChannels() == 2;
        int windowSize = fftParameters.TamanoVentanaMuestreo;
        Tipo_Onda windowFunction = fftParameters.tipoOnda;
        double overlap = fftParameters.TraslapeDeVentana;
        int zeroPadLength = fftParameters.zeroPadLength();

        double startTimeMs = samplesTakenCount * windowDurationMs * (1 - fftParameters.TraslapeDeVentana);
        float sampleRate = audioReader.ObtenFormatoDeAudio().getSampleRate();

        SampleWindowExtractor windowExtractor = new SampleWindowExtractor(nextWindow, isStereo, windowSize,
                windowFunction, overlap, zeroPadLength);
        nextWindow = windowExtractor.convertSamplesToWindow(nextWindow);

        samplesTakenCount++;

        CuadroFFT nextFrame = FFTComputationWrapper.doFFT(nextWindow, startTimeMs,
                windowDurationMs, fileDurationMs, sampleRate, fftParameters);
        if(fftParameters.UsarEscalaDecibeles) {
            FFTComputationWrapper.scaleLogarithmically(nextFrame);
        }

        return nextFrame;
    }

    @Override
    public void setMetadata(AudioReader reader, ParametrosFFT params) {
        super.setMetadata(reader, params);

        // capture AudioReader object after setting metadata
        audioReader = reader;
        audioReader.setFFTParameters(params);
    }
}
