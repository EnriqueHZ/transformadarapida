/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pruebafft;

import org.quifft.AudioAFFT;
import org.quifft.output.CuadroFFT;
import org.quifft.output.ResultadosFFT;
import org.quifft.output.FrequencyBin;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import javafx.scene.chart.XYChart;

public class FFT_FromAudio {

    private File sineWave600Hz;
    HBox root = null;
    Scene scene = null;
    NumberAxis xAxis = null;
    NumberAxis yAxis = null;
    CuadroFFT[] FftFrames = null;
    static String[] arg = null;
    /*public static void main(String[] args) {
        FFT_FromAudio firstFFT = null;
        try{
            //arg = args;
             firstFFT = new FFT_FromAudio();
             
        } catch (Exception e){
            System.out.println("Error creating  FFT contructor: " + e.getMessage());
        }
        
        firstFFT.computeDefaultFFT();
        //launch(args);
    }*/

    private void computeDefaultFFT() {
        // compute an FFT with AudioAFFT's default settings
        ResultadosFFT fft = null;
        try {
            AudioAFFT quiFFT = new AudioAFFT(sineWave600Hz);
            quiFFT.DefineEscala(false);
            fft = quiFFT.CalcularFFT();
        } 
        catch (IOException e) {
            System.out.println("An I/O exception occurred while QuiFFT was opening an input stream to the audio file");
            System.out.println("Error: " + e.getMessage());
        } catch (UnsupportedAudioFileException e) {
            System.out.println("QuiFFT was given an invalid audio file");
            System.out.println("Error: " + e.getMessage());
        }

        // print the ResultadosFFT to see details about the transformation and the audio file on which it was performed
        System.out.println(fft);

        // get individual frames (sampling windows) from FFT
        FftFrames = fft.fftFrames;
        System.out.println("There are " + FftFrames.length + " frames in this FFT, each of which was computed " +
                "from a sampling window that was about " + Math.round(fft.windowDurationMs) + " milliseconds long.");
        
        
        for(int i = 0; i < FftFrames.length; i++){
            
            CuadroFFT cuadro = FftFrames[i];
            double frec = FftFrames[i].bins[i].frequency;
            double amp = FftFrames[i].bins[i].amplitude;
            System.out.println("Frec: " + frec + " | amp " + amp );
            //graficaFFT.getData().add(new XYChart.Data<Number, Number>(frec, amp));
        }

        // inspect amplitudes of individual frequency bins in the first frame
        FrequencyBin firstBin = FftFrames[0].bins[0];
        System.out.println("The first bin, located at " + Math.round(firstBin.frequency) + " Hz, has an amplitude of " +
                Math.round(firstBin.amplitude) + " dB.");
        FrequencyBin mostPowerfulBin = FftFrames[0].bins[56]; // bin closest to 600 Hz
        System.out.println("The 56th bin, located at " + Math.round(mostPowerfulBin.frequency) + " Hz, has an " +
                "amplitude of " + Math.round(mostPowerfulBin.amplitude) + " dB.");
        
    
    
    }


    public  FFT_FromAudio() {
        ClassLoader classLoader = getClass().getClassLoader();
        //sineWave600Hz = new File(classLoader.getResource("600hz-sine-wave.wav").getFile()); pruebaAudio
        //sineWave600Hz = new File(classLoader.getResource("500hz-tone-3secs-mono.wav").getFile());
        //sineWave600Hz = new File(classLoader.getResource("600hz-tone-3secs-mono.wav").getFile());
        sineWave600Hz = new File(classLoader.getResource("barrido.wav").getFile());
        
        computeDefaultFFT();
    }

}