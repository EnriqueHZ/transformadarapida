/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebafft;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

/**
 *
 * @author rike
 */

public class GUI extends JFrame{
	
    //  constants
	
    static int  N = 1;
    //static int  WIDTH  = 650;
    
    //static int  HEIGHT = 600;
    //static int  SHIFT_X = 10;
    static int  WIDTH  = 1000;
    static int  HEIGHT = 300;
    static int  SLIDERMINVALUE = 1;
    static int  PIXELS_PER_XYUNIT = (int) (WIDTH / N);
    static int  SHIFT_X = 10;
    static int  SHIFT_Y = 10;   //  shift plot away from left border
	
    //  GUI (Graphical user interface) variables

    static JLabel view;
    static JPanel panel1, panel2, panel3;
    static BufferedImage surface;
    static Graphics g;
    static Point2D[] coords;       //  (x,y) values  

//  constructor
	
    public  GUI(){

		
    panel1 = new JPanel();
    panel2 = new JPanel();
    add(panel2, BorderLayout.CENTER); 

    //   The third panel shows the points and line fits.
		
    surface = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
    g = surface.getGraphics();
    g.setColor(Color.BLACK);
    g.fillRect(0,0,WIDTH,HEIGHT);
    view = new JLabel(new ImageIcon(surface));

    panel3 = new JPanel();
    panel3.add(view);
    add(panel3, BorderLayout.SOUTH); 

    setSize(WIDTH + 100,HEIGHT + 100);    //  make the window bigger than the two panels so there's a border
		
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setVisible(true);
    setResizable(false);		
    redraw();
}


	
    public static void redraw(){ 
		
	int ovalwidth = 8;
	int ovalradius = ovalwidth/2;
	g.setColor(new Color(220, 220, 220));   //  background color
	g.fillRect(0,0,WIDTH,HEIGHT);
	view.repaint();
	g.setColor(Color.BLACK);
		
	//  draw points (xi, yi)
	/*	
	for(int i = 0;i<N;i++)		{
            g.fillOval( 
                    SHIFT_X + (int)( coords[i].getX()*PIXELS_PER_XYUNIT ) - ovalradius, 
            (int)(HEIGHT - (coords[i].getY() *PIXELS_PER_XYUNIT)) - ovalradius  - SHIFT_Y, 
	    ovalwidth, ovalwidth ); 
	}*/
        
        for(int i = 0;i<N;i++)		{
            g.fillOval( 
            SHIFT_X + (int)( coords[i].getX()) - ovalradius, 
            (int)(HEIGHT - (coords[i].getY())) - ovalradius  - SHIFT_Y, 
	    ovalwidth, ovalwidth ); 
	}
        
    }
    
    
    public static void main(String[] args) {
        int  costSegment = 10; //  Implement a slider to allow user to adjust this interactively.
		
         
	
        FFT_FromAudio fft = new FFT_FromAudio();
	N = fft.FftFrames.length;	
        PIXELS_PER_XYUNIT = (int) (WIDTH / N);
        
        Point2D[] points = new Point2D[N];   
	for (int i = 0;  i < N; i++){            
            points[i] = new Point2D.Float();
            points[i].setLocation(fft.FftFrames[i].bins[i].frequency, fft.FftFrames[i].bins[i].amplitude);  
	}		
	
	coords = points;		
	new GUI();
    }
    
}
